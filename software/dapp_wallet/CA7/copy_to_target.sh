#!/bin/bash

# Accept an optional argument for the target address (default is stm32mp1.local)
# - Note if using a static IP via Ethernet, the IP is likely 192.168.7.2
TARGET_ADDRESS=${1:-stm32mp1.local}

HOST_FILE=trustedTransactionSigner/host/trustedTransactionSigner
TA_FILE=trustedTransactionSigner/ta/1af1a4cc-0912-48d5-8b63-a0f564afb197.ta
WORDLIST_FILE=trustedTransactionSigner/host/bip0039_english.txt

scp fw_cortex_m4.sh root@$TARGET_ADDRESS:~
scp $HOST_FILE root@$TARGET_ADDRESS:~
scp $TA_FILE root@$TARGET_ADDRESS:/lib/optee_armtz/
scp $WORDLIST_FILE root@$TARGET_ADDRESS:~
