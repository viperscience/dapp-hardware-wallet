#!/bin/bash

# This script is run from the A7 core to do the following:
# 1. Load the firmware to the M4 core
# 2. Start the firmware on the M4 core
# 3. Open the RPMSG channel between the A7 and M4 cores
# 4. Start the user application on the A7 core, which will communicate with the TA in OP-TEE

rproc_class_dir="/sys/class/remoteproc/remoteproc0/"
fmw_dir="/lib/firmware"
rproc_ta="optee-ta-1af1a4cc-0912-48d5-8b63-a0f564afb197"
project_name=dapp_wallet_CM4
fmw_name="${project_name}.elf"
if [ -e /sys/bus/tee/devices/${rproc_ta} ]; then
    #The firmware is managed by OP-TEE, it must be signed.
    if [ ! -e lib/firmware/${project_name}.sig ] && [ $1 == "start" ]; then
        echo  "Error: only signed firmware can be loaded"
        exit 1
    fi
    fmw_name="${project_name}.sig"
fi
rproc_state=`tr -d '\0' < $rproc_class_dir/state`

error() {
	echo -e "$1"
	exit 0
}


case $1 in
	start) ;;
	stop) ;;
	*) echo "`basename ${0}`:usage: start | stop"
	   exit 1
	   ;;
esac

#################
# Start example #
#################
if [ $1 == "start" ]
then

if [ $rproc_state == "running" ]
then
echo stop > $rproc_class_dir/state
fi

# load and start firmware
echo $fmw_name > $rproc_class_dir/firmware
echo start > $rproc_class_dir/state
/home/root/trustedTransactionSigner
#sleep 1
#stty -onlcr -echo -F /dev/ttyRPMSG0
#sleep 1
echo " " > /dev/ttyRPMSG0
sleep 1

fi


################
# Stop example #
################
if [ $1 == "stop" ]
then

if [ $rproc_state == "offline" ]
then
echo "Nothing to do, no M4 fw is running"

else
echo stop > $rproc_class_dir/state

fi
fi

