#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>

#include <botan/hash.h>
#include "qcbor/qcbor_decode.h"

#include <tee_client_api.h>
#include <trusted_signer_ta.h>

#define MAX_READ 512 * 64
#define SHA512_DIGEST_SIZE 64
#define TX_ID_SIZE 32

// Set to 1 to coordinate signing with GUI
#define USING_GUI 1
#define GUI_TIMEOUT_SECONDS 15
#define SOURCE_WALLET_ADDR "60B7FD91419F7BF941BC37D5ADA73D901128C9D7079B8A5E8A0A63430F"
#define WALLET_ADDR_SIZE 57

bool check_packet(uint8_t *rx_buf, size_t rx_buf_size, size_t i, size_t packet_size);
uint8_t **parse_rx_buffer(uint8_t *rx_buf, ssize_t *rx_buf_size, size_t *num_packets);
void assemble_transaction(uint8_t **packets, size_t num_packets, uint8_t *tx, size_t *tx_size);
char* hexStringToByteArray(const char* hexString);
bool parse_cbor_tx(uint8_t *cbor_bytes, size_t n_bytes, uint8_t *wallet_byte_str, uint32_t *lovelace, uint32_t *tokens);
void generate_key();
void sign_transaction(unsigned char *tx_id, unsigned char *signature);

int main(int argc, char *argv[])
{
	uint8_t signature[SHA512_DIGEST_SIZE];
	uint8_t rx_buf[MAX_READ];
	uint8_t tx_id[TX_ID_SIZE];
	int fd;
	ssize_t count = 0;

	if (argc > 1 && std::string(argv[1]) == "--generate-key")
	{
		std::printf("\n,------------------------------------------------------,\n");
		std::printf("| Generating new wallet key...                         |\n");
		std::printf("'------------------------------------------------------'\n");
		generate_key();
		return 0;
	}

	std::printf("\n,------------------------------------------------------,\n");
	std::printf("| Hello, Cardano! I am a Hardware dApp Wallet          |\n");
	std::printf("| designed to secure the wallet keys of live dApp      |\n");
	std::printf("| services like dexes, bridges, NFT minting, and more! |\n");
	std::printf("'------------------------------------------------------'\n");

	// Open /dev/ttyRPMSG0 for UART communication with CM4
	fd = open("/dev/ttyRPMSG0", O_RDWR);
	if (fd < 0)
	{
		std::printf("Failed to open /dev/ttyRPMSG0\n");
		return -1;
	}

	// Configure RPMSG0 UART
	struct termios options;
	tcgetattr(fd, &options);
	options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &options);

	// Send a byte to CM4 to open the RPMSG channel
	write(fd, "0", 1);

	std::printf("\nWaiting for transaction...\n");

	// Poll UART for transaction ID
	while (1)
	{

		// Get input from CM4
		count = read(fd, &rx_buf[count], sizeof(rx_buf) - count - 1);

		if (count == -1)
		{
			perror("read");
			close(fd);
			return 1;
		} else if (count == 0) {
			continue;
		}

		// Print rx_buf hex
		printf("\nReceived the following from CM4 UART:\n");
		for (int i = 0; i < count; i++)
		{
			std::printf("%02x", rx_buf[i]);
		}
		std::printf("\n");

		// Parse any packets in the buffer
		size_t num_packets;
		uint8_t **packets = parse_rx_buffer(rx_buf, &count, &num_packets);

		// Check if there are any packets in the buffer
		if (num_packets == 0)
		{
			std::printf("\nNo packets found in buffer\n");
			continue;
		}

		// Check if the buffer contains a full packet
		if (packets == NULL)
		{
			std::printf("\nIncomplete packet found in buffer\n");
			continue;
		}

		// Check if the buffer contains multiple packets
		if (num_packets > 0)
		{
			std::printf("\n%d packets found in buffer\n", num_packets);
		}

		// Check new size of rx_buf
		std::printf("\nNew size of rx_buf: %d\n", count);

		// Assemble the transaction from the packets
		uint8_t tx[MAX_READ];
		size_t tx_size;
		assemble_transaction(packets, num_packets, tx, &tx_size);
		std::printf("\nAssembled transaction (size: %d): \n", tx_size);
		for (uint16_t i = 0; i < tx_size; i++)
		{
			std::printf("%02x", tx[i]);
		}

		// Calculate tx_id hash
		const auto blake2b = Botan::HashFunction::create("Blake2b(256)");
		blake2b->update(tx, tx_size);
		auto result = blake2b->final();
		std::memcpy(tx_id, result.data(), TX_ID_SIZE);

		std::stringstream tx_id_str;
		tx_id_str << std::hex << std::setfill('0');
		for (int i = 0; i < TX_ID_SIZE; i++)
		{
			tx_id_str << std::setw(2) << static_cast<unsigned>(tx_id[i]);
		}
		std::string tx_id_string = tx_id_str.str();

		#if USING_GUI
			// Write TX to file for GUI to approve
			FILE *fp;
			uint8_t tx_found = 0;
			uint8_t approved = 0;
			std::string line;

			uint8_t *addr_bytes = (uint8_t *)malloc(WALLET_ADDR_SIZE * sizeof(uint8_t));
			uint32_t lovelace;
			uint32_t tokens;
			parse_cbor_tx(tx, tx_size, addr_bytes, &lovelace, &tokens);

			fp = fopen("/tmp/incoming_txs.csv", "a");
			if (fp == NULL)
			{
				perror("fopen");
				close(fd);
				return 1;
			}

			// Write row to file (tx_id, address, ada, tokens)
			for (int i = 0; i < TX_ID_SIZE; i++)
			{
				fprintf(fp, "%02x", tx_id[i]);
			}
			 fprintf(fp, ",");
			 for (int i = 0; i < WALLET_ADDR_SIZE; i++) {
			 	fprintf(fp, "%02x", addr_bytes[i]);
			 }
			fprintf(fp, ",%u,%u\n", lovelace, tokens);
			fclose(fp);

			// Wait for GUI to approve transaction
			std::printf("\nWaiting for GUI to approve transaction...\n");
			auto start = std::chrono::high_resolution_clock::now();
			while (1)
			{
				std::ifstream fp("/tmp/processed_txs.csv");
				if (fp)
				{
					// Read the file line by line
					while (std::getline(fp, line))
					{
						std::printf("Read line from processed_txs.csv: %s\n", line.c_str());
						std::printf("Searching for tx_id (%s) in line...\n", tx_id_string.c_str());
						if (line.find(tx_id_string) != std::string::npos)
						{
							std::printf("\nFound transaction in processed_txs.csv\n");
							tx_found = true;
							// If the line begins with the tx_id, check if it was approved
							if (line.find("approved") != std::string::npos)
							{
								approved = true;
							}
							else
							{
								approved = false;
							}
							break;
						}
					}

					// Close the file
					fp.close();
				}

				if (tx_found)
				{
					break;
				}

				auto end = std::chrono::high_resolution_clock::now();
				auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start);

				if (elapsed.count() >= GUI_TIMEOUT_SECONDS)
				{
					std::printf("\nTimeout reached, transaction halted.\n");
					approved = 0;
					break;
				}

				if (tx_found) {
					break;
				}

				// Sleep for 500 ms
				usleep(500000);
			}

			if (approved)
			{
				std::printf("\nTransaction approved by GUI\n");
			} 
			else
			{
				std::printf("\nTransaction not approved by GUI\n");
				// Write SHA512_DIGEST_SIZE 0s to signature buffer
				for (int i = 0; i < SHA512_DIGEST_SIZE; i++)
				{
					signature[i] = 0;
				}

				// Send NULL signature back to CM4
				write(fd, signature, SHA512_DIGEST_SIZE);

				// Flush the buffer
				tcdrain(fd);
				tcflush(fd, TCIOFLUSH);

				std::printf("\nSent NULL signature back to CM4\n");
				continue;
			}
		#endif

		sign_transaction(tx_id, signature);
		printf("\nTrusted Application successfully signed transaction w/ signature: \n");
		for (int i = 0; i < SHA512_DIGEST_SIZE; i++)
		{
			printf("%02x", signature[i]);
		}

		// Send signature back to CM4
		write(fd, signature, SHA512_DIGEST_SIZE);

		// Flush the buffer
		tcdrain(fd);
		tcflush(fd, TCIOFLUSH);

		printf("\nSent signature back to CM4\n");
	}

	return 0;
}

bool check_packet(uint8_t *rx_buf, size_t rx_buf_size, size_t i, size_t packet_size) {
	// Check if the remaining bytes in the buffer are enough to contain the full packet
	if (i + packet_size > rx_buf_size) {
		// Incomplete packet in buffer, replace rx_buf with the partial packet
		size_t remaining_bytes = rx_buf_size - i;
		uint8_t *partial_packet = (uint8_t *)malloc(remaining_bytes * sizeof(uint8_t));
		if (partial_packet == NULL) {
			throw std::runtime_error("Failed to allocate memory for partial packet");
		}
		std::memcpy(partial_packet, &rx_buf[i], remaining_bytes);
		std::memcpy(rx_buf, partial_packet, remaining_bytes);
		free(partial_packet);
		return false;
	}
	return true;
}

/* Create a function to parse multiple packets from rx_buf
*  Should dynamically allocate memory for each of the packets
*  and return a pointer to the array of packets
*  Any bytes left over should be returned in a separate buffer
*
*  The packet format is as follows:
*   - 2 bytes: packet header (0x54F7)
*   - 1 byte: payload length (<= 250B)
*   - 2 bytes: packets remaining (<= 63)
*   - 1 - 251 bytes: packet payload
*   - 1 byte: packet checksum (XOR of header & payload bytes)
*
*    +------------------------+
*    |   Header 0x54F7 (2B)   |
*    +------------------------+
*    |   Packet Length (1B)   |
*    +------------------------+
*    | Packets Remaining (1B) |
*    +------------------------+
*    |  Packet Data (<=251B)  |
*    +------------------------+
*    |   Packet Checksum (1B) |
*    +------------------------+
*/
uint8_t **parse_rx_buffer(uint8_t *rx_buf, ssize_t *rx_buf_size, size_t *num_packets)
{
	size_t packet_index = 0;
	size_t packet_size = 0;
	size_t packet_payload_size = 0;
	size_t packet_checksum = 0;
	bool partial_packet = false;
	*num_packets = 0;

	// Count the number of packets in the current transaction:
	// 1. Find the first packet header
	// 2. Read the number of packets remaining from the packet
	ssize_t i;
	for (i = 0; i < *rx_buf_size; i++)
	{
		if (rx_buf[i] == 0x54 && rx_buf[i + 1] == 0xF7)
		{
			*num_packets = rx_buf[i + 3] + 1; // Add 1 to account for the current packet
			break;
		}
	}

	std::printf("Number of packets in buffer: %d\n", *num_packets);
	uint8_t **packets = (uint8_t **)malloc(*num_packets * sizeof(uint8_t *));	
	if (packets == NULL)
	{
		throw std::runtime_error("Failed to allocate memory for packets");
	}

	// Parse the packets
	for (i = 0; i < *rx_buf_size; i++)
	{
		if (rx_buf[i] == 0x54 && rx_buf[i + 1] == 0xF7)
		{
			// Get the packet length
			packet_payload_size = rx_buf[i + 2];
			packet_size = packet_payload_size + 5;

			// Get the packet checksum
			packet_checksum = rx_buf[i + packet_size - 1];

			// Calculate the checksum
			uint8_t checksum = 0;
			for (size_t j = 0; j < packet_size - 1; j++)
			{
				checksum ^= rx_buf[i + j];
			}

			// Check if the checksum is valid
			if (checksum != packet_checksum)
			{
				std::printf("Invalid checksum for packet %d\n", packet_index);
				continue;
			}

			// Allocate memory for the packet
			packets[packet_index] = (uint8_t *)malloc(packet_size * sizeof(uint8_t));
			if (packets[packet_index] == NULL)
			{
				throw std::runtime_error("Failed to allocate memory for packet");
			}

			if (!check_packet(rx_buf, *rx_buf_size, i, packet_size)) {
				partial_packet = true;
				break;
			}

			std::printf("Packet %d found at index %d\n", packet_index, i);

			// Copy the packet to the packets array
			std::memcpy(packets[packet_index], &rx_buf[i], packet_size);

			// Increment the packet index
			packet_index++;
		}
	}

	// If there are any bytes left over, replace rx_buf with the partial packet
	if (partial_packet)
	{
		size_t remaining_bytes = *rx_buf_size - i;
		// Move the partial packet to the beginning of the buffer
		std::memmove(rx_buf, &rx_buf[i], remaining_bytes);
		*rx_buf_size = remaining_bytes;
	}
	else
	{
		*rx_buf_size = 0;
	}
	return packets;
}

// Assemble a transaction by merging the payload of each packet
void assemble_transaction(uint8_t **packets, size_t num_packets, uint8_t *tx, size_t *tx_size)
{
	*tx_size = 0;
	for (size_t i = 0; i < num_packets; i++)
	{
		// Get the packet payload size
		size_t packet_payload_size = packets[i][2];

		// Copy the packet payload to the transaction
		std::memcpy(&tx[*tx_size], &packets[i][4], packet_payload_size);

		// Increment the transaction index
		*tx_size += packet_payload_size;
	}
}

char* hexStringToByteArray(const char* hexString)
{
    size_t len = strlen(hexString);
    size_t byteLen = len / 2;
    char* byteArray = new char[byteLen];

    for (size_t i = 0; i < byteLen; i++)
    {
        sscanf(hexString + 2 * i, "%2hhx", &byteArray[i]);
    }

    return byteArray;
}

bool parse_cbor_tx(uint8_t *cbor_bytes, size_t n_bytes, uint8_t *wallet_byte_str, uint32_t *lovelace, uint32_t *tokens)
{
    bool is_valid;

    QCBORDecodeContext pCtx;
    QCBORError error;
    UsefulBufC tx_buf = {cbor_bytes, n_bytes};

    QCBORDecode_Init(&pCtx, tx_buf, QCBOR_DECODE_MODE_NORMAL);

    QCBORItem item;
    uint8_t last_item_type = QCBOR_TYPE_NULL;
    bool is_dest_wallet;
    bool found_dest_wallet_byte_str = false;
	bool parsed_amounts = false;
    char* source_wallet_bytes = hexStringToByteArray(SOURCE_WALLET_ADDR);
    for (;;)
    {
        QCBORDecode_VGetNext(&pCtx, &item);
        switch (item.uDataType)
        {
        case QCBOR_TYPE_INT64:
			if (parsed_amounts) {
				break;
			}

            if (found_dest_wallet_byte_str && last_item_type == QCBOR_TYPE_ARRAY)
            {
                *lovelace = (uint32_t)item.val.int64;
            }
            else if (found_dest_wallet_byte_str && last_item_type == QCBOR_TYPE_MAP)
            {
                *tokens = (uint32_t)item.val.int64;
				parsed_amounts = true;
            }
            break;
        case QCBOR_TYPE_BYTE_STRING:
            if (item.val.string.len < WALLET_ADDR_SIZE) {
                break;
            }

            // Check if the byte string is the wallet byte string
            is_dest_wallet = false;
            for (size_t i = 0; i < item.val.string.len; i++)
            {
                if (((uint8_t *)item.val.string.ptr)[i] != (uint8_t)source_wallet_bytes[i])
                {
                    is_dest_wallet = true;
                    break;
                }
            }

            // If it's not our wallet, we're ready to look for coin/token amounts coming up
            if (is_dest_wallet)
            {
                found_dest_wallet_byte_str = true;
                // wallet_byte_str = (uint8_t *)item.val.string.ptr;
				memcpy(wallet_byte_str, (uint8_t *)item.val.string.ptr, WALLET_ADDR_SIZE);
            } else
            {
                found_dest_wallet_byte_str = false;
            }
            break;
        default:
            break;
        }
        if (item.uDataType == QCBOR_TYPE_NONE)
        {
            // Either an error or done parsing
            error = QCBORDecode_Finish(&pCtx);
            if (error != QCBOR_ERR_NO_MORE_ITEMS)
            {
                is_valid = false;
                break;
            }
            else
            {
                is_valid = true;
                break;
            }
        }
        last_item_type = item.uDataType;
    }

    // Print address, lovelace, and token amounts
    printf("\n\nAddress: ");
    for (size_t i = 0; i < WALLET_ADDR_SIZE; i++)
    {
        printf("%02X", wallet_byte_str[i]);
    }
    printf("\n");
    printf("Lovelace: %u\n", *lovelace);
    printf("Token: %u\n", *tokens);

    return is_valid;
}

void generate_key() {
	TEEC_Result res;
	TEEC_Context ctx;
	TEEC_Session sess;
	TEEC_Operation op;
	TEEC_UUID uuid = TA_TRUSTED_SIGNER_UUID;
	uint32_t err_origin;
	const uint8_t MNEMONIC_SIZE = 24;
	uint16_t mnemonic[MNEMONIC_SIZE];

	/* Initialize a context connecting us to the TEE */
	res = TEEC_InitializeContext(NULL, &ctx);
	if (res != TEEC_SUCCESS)
		throw std::runtime_error("TEEC_InitializeContext failed with code 0x" + std::to_string(res));
	/*
	 * Open a session to the TA
	 */
	res = TEEC_OpenSession(&ctx, &sess, &uuid,
						   TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
	if (res != TEEC_SUCCESS)
		throw std::runtime_error("TEEC_Opensession failed with code 0x" + std::to_string(res) + " origin 0x" + std::to_string(err_origin));

	/* Clear the TEEC_Operation struct */
	memset(&op, 0, sizeof(op));

	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_OUTPUT, TEEC_NONE,
									 TEEC_NONE, TEEC_NONE);

	// Use shared memory to receive mnemonic indices from TA
	op.params[0].tmpref.buffer = malloc(MNEMONIC_SIZE * 2);
	op.params[0].tmpref.size = MNEMONIC_SIZE * 2;

	// Invoke TA to generate key
	res = TEEC_InvokeCommand(&sess, TA_TRUSTED_SIGNER_CMD_GENERATE_KEY, &op,
							 &err_origin);
	if (res != TEEC_SUCCESS)
		throw std::runtime_error("TEEC_InvokeCommand failed with code 0x" + std::to_string(res) + " origin 0x" + std::to_string(err_origin));

	/*
	 * We're done with the TA, close the session and
	 * destroy the context.
	 */
	TEEC_CloseSession(&sess);
	TEEC_FinalizeContext(&ctx);

	// Copy the mnemonic indices from TA to mnemonic buffer
	std::memcpy(mnemonic, op.params[0].tmpref.buffer, MNEMONIC_SIZE * 2);

	// Print the mnemonic indices
	std::printf("\nMnemonic indices: ");
	for (int i = 0; i < MNEMONIC_SIZE; i++)
	{
		std::printf("%04x, ", mnemonic[i]);
	}

	// Look up the mnemonic words from the indices from a word list
	std::ifstream wordlist("bip0039_english.txt");
	std::string word;
	std::string mnemonic_words[MNEMONIC_SIZE];
	if (!wordlist.is_open())
	{
		throw std::runtime_error("Failed to open word list");
	}

	for (int i = 0; i < MNEMONIC_SIZE; i++)
	{
		wordlist.seekg(0, std::ios::beg);
		for (int j = 0; j <= mnemonic[i]; j++)
		{
			std::getline(wordlist, word);
		}
		mnemonic_words[i] = word;
	}

	// Print the mnemonic words
	std::printf("\nMnemonic words: ");
	for (int i = 0; i < MNEMONIC_SIZE; i++)
	{
		std::printf("%s ", mnemonic_words[i].c_str());
	}

	// Write the mnemonic words to a temp file
	std::ofstream mnemonic_file("/mnt/tmpfs/mnemonic.txt");
	if (!mnemonic_file.is_open())
	{
		throw std::runtime_error("Failed to open mnemonic file");
	}
	for (int i = 0; i < MNEMONIC_SIZE; i++)
	{
		mnemonic_file << mnemonic_words[i] << " ";
	}
	mnemonic_file.close();

	// Free the shared memory reference
	free(op.params[0].tmpref.buffer);

	return;
}

void sign_transaction(uint8_t *tx_id, uint8_t *signature)
{
	TEEC_Result res;
	TEEC_Context ctx;
	TEEC_Session sess;
	TEEC_Operation op;
	TEEC_UUID uuid = TA_TRUSTED_SIGNER_UUID;
	uint32_t err_origin;

	/* Initialize a context connecting us to the TEE */
	res = TEEC_InitializeContext(NULL, &ctx);
	if (res != TEEC_SUCCESS)
		throw std::runtime_error("TEEC_InitializeContext failed with code 0x" + std::to_string(res));
	/*
	 * Open a session to the TA
	 */
	res = TEEC_OpenSession(&ctx, &sess, &uuid,
						   TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
	if (res != TEEC_SUCCESS)
		throw std::runtime_error("TEEC_Opensession failed with code 0x" + std::to_string(res) + " origin 0x" + std::to_string(err_origin));

	/* Clear the TEEC_Operation struct */
	memset(&op, 0, sizeof(op));

	std::printf("\nInvoking Trusted Application to sign transaction: \n");
	for (int i = 0; i < TX_ID_SIZE; i++)
	{
		std::printf("%02x", tx_id[i]);
	}
	std::printf("\n");

	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_MEMREF_TEMP_OUTPUT,
									 TEEC_NONE, TEEC_NONE);

	// Use a shared memory reference to pass tx_id to TA
	op.params[0].tmpref.buffer = tx_id;
	op.params[0].tmpref.size = TX_ID_SIZE;
	op.params[1].tmpref.buffer = malloc(SHA512_DIGEST_SIZE);
	op.params[1].tmpref.size = SHA512_DIGEST_SIZE;

	// Invoke TA to sign transaction
	res = TEEC_InvokeCommand(&sess, TA_TRUSTED_SIGNER_CMD_SIGN_TX, &op,
							 &err_origin);
	if (res != TEEC_SUCCESS)
		throw std::runtime_error("TEEC_InvokeCommand failed with code 0x" + std::to_string(res) + " origin 0x" + std::to_string(err_origin));

	/*
	 * We're done with the TA, close the session and
	 * destroy the context.
	 */
	TEEC_CloseSession(&sess);
	TEEC_FinalizeContext(&ctx);

	// Copy signature from TA to signature buffer
	std::memcpy(signature, op.params[1].tmpref.buffer, SHA512_DIGEST_SIZE);

	// Free the shared memory reference
	free(op.params[1].tmpref.buffer);

	return;
}
