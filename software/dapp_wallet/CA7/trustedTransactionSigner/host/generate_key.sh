#!/bin/bash

# Create a RAM disk to store the key in a temporary file
mkdir -p /mnt/tmpfs
mount -o size=1M -t tmpfs none /mnt/tmpfs

# Generate the key
./trustedTransactionSigner --generate-key