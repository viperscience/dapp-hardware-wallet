
release_major = 3
release_minor = 1
release_patch = 1
release_suffix = ''
release_so_abi_rev = 0

# These are set by the distribution script
release_vc_rev = 'git:f60608b8818c7bb8579fe797122ed6116f4134af'
release_datestamp = 20230713
release_type = 'release'
