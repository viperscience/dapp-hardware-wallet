
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/willie/projects/viper/hw-wallet/dapp-hardware-wallet/software/dapp_wallet/CA7/trustedTransactionSigner/host/QCBOR/src/UsefulBuf.c" "CMakeFiles/qcbor.dir/src/UsefulBuf.c.o" "gcc" "CMakeFiles/qcbor.dir/src/UsefulBuf.c.o.d"
  "/home/willie/projects/viper/hw-wallet/dapp-hardware-wallet/software/dapp_wallet/CA7/trustedTransactionSigner/host/QCBOR/src/ieee754.c" "CMakeFiles/qcbor.dir/src/ieee754.c.o" "gcc" "CMakeFiles/qcbor.dir/src/ieee754.c.o.d"
  "/home/willie/projects/viper/hw-wallet/dapp-hardware-wallet/software/dapp_wallet/CA7/trustedTransactionSigner/host/QCBOR/src/qcbor_decode.c" "CMakeFiles/qcbor.dir/src/qcbor_decode.c.o" "gcc" "CMakeFiles/qcbor.dir/src/qcbor_decode.c.o.d"
  "/home/willie/projects/viper/hw-wallet/dapp-hardware-wallet/software/dapp_wallet/CA7/trustedTransactionSigner/host/QCBOR/src/qcbor_encode.c" "CMakeFiles/qcbor.dir/src/qcbor_encode.c.o" "gcc" "CMakeFiles/qcbor.dir/src/qcbor_encode.c.o.d"
  "/home/willie/projects/viper/hw-wallet/dapp-hardware-wallet/software/dapp_wallet/CA7/trustedTransactionSigner/host/QCBOR/src/qcbor_err_to_str.c" "CMakeFiles/qcbor.dir/src/qcbor_err_to_str.c.o" "gcc" "CMakeFiles/qcbor.dir/src/qcbor_err_to_str.c.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
