global-incdirs-y += include ed25519
srcs-y += trusted_signer_ta.c sha2_impl.c cryptonite_chacha.c ed25519/ed25519.c fastpbkdf2.c

# To remove a certain compiler flag, add a line like this
#cflags-template_ta.c-y += -Wno-strict-prototypes
