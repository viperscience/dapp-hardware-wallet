#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <trusted_signer_ta.h>
#include <fastpbkdf2.h>
#include <sha2_impl.h>

#include "ed25519/ed25519.h"

#define SEED_SIZE 32
#define MNEMONIC_SIZE 24
#define SECRET_KEY_SIZE 64
#define PUBLIC_KEY_SIZE 32
#define CHAIN_CODE_SIZE 32
#define FULL_KEY_SIZE (SECRET_KEY_SIZE + PUBLIC_KEY_SIZE + CHAIN_CODE_SIZE)
#define TX_ID_SIZE 32

void hex_string_to_byte_array(const char *hex_string, uint8_t *byte_array);

/*
 * Called when the instance of the TA is created. This is the first call in
 * the TA.
 */
TEE_Result TA_CreateEntryPoint(void)
{
	DMSG("has been called");

	return TEE_SUCCESS;
}

/*
 * Called when the instance of the TA is destroyed if the TA has not
 * crashed or panicked. This is the last call in the TA.
 */
void TA_DestroyEntryPoint(void)
{
	DMSG("has been called");
}

/*
 * Called when a new session is opened to the TA. *sess_ctx can be updated
 * with a value to be able to identify this session in subsequent calls to the
 * TA. In this function you will normally do the global initialization for the
 * TA.
 */
TEE_Result TA_OpenSessionEntryPoint(uint32_t param_types,
									TEE_Param __maybe_unused params[4],
									void __maybe_unused **sess_ctx)
{
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE,
											   TEE_PARAM_TYPE_NONE,
											   TEE_PARAM_TYPE_NONE,
											   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	/* Unused parameters */
	(void)&params;
	(void)&sess_ctx;

	/*
	 * The DMSG() macro is non-standard, TEE Internal API doesn't
	 * specify any means to logging from a TA.
	 */
	IMSG("Hello from the Trusted Execution Environment! This is where wallet keys can be securely handled.\n");

	/* If return value != TEE_SUCCESS the session will not be created. */
	return TEE_SUCCESS;
}

/*
 * Called when a session is closed, sess_ctx hold the value that was
 * assigned by TA_OpenSessionEntryPoint().
 */
void TA_CloseSessionEntryPoint(void __maybe_unused *sess_ctx)
{
	(void)&sess_ctx; /* Unused parameter */
	IMSG("Goodbye!\n");
}

static TEE_Result sign_tx(uint32_t param_types,
						  TEE_Param params[4])
{
	char txid_string[2 * TX_ID_SIZE + 1];
	char sig_string[2 * SHA512_DIGEST_SIZE + 1];
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
											   TEE_PARAM_TYPE_MEMREF_OUTPUT,
											   TEE_PARAM_TYPE_NONE,
											   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	unsigned char *tx_id = params[0].memref.buffer;

	for (int i = 0; i < TX_ID_SIZE; i++)
	{
		sprintf(txid_string + 2 * i, "%02x", tx_id[i]);
	}
	txid_string[2 * TX_ID_SIZE] = '\0';
	IMSG("Securely signing transaction ID: %s\n", txid_string);

	// Load hw subkey
	TEE_ObjectHandle persistent_key = (TEE_ObjectHandle)NULL;
	TEE_Result res = TEE_ERROR_GENERIC;
	uint32_t flags = TEE_DATA_FLAG_ACCESS_READ | TEE_DATA_FLAG_ACCESS_WRITE | TEE_DATA_FLAG_ACCESS_WRITE_META | TEE_DATA_FLAG_SHARE_READ | TEE_DATA_FLAG_SHARE_WRITE;
	uint32_t objectID = 1;

	res = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE, &objectID, sizeof(objectID), flags, &persistent_key);
	if (res != TEE_SUCCESS)
	{
		IMSG("Failed to open persistent key: 0x%x", res);
		return res;
	}

	// Print the public key
	uint8_t full_key[FULL_KEY_SIZE];
	size_t key_size_bytes = FULL_KEY_SIZE;
	res = TEE_GetObjectBufferAttribute(persistent_key, TEE_ATTR_SECRET_VALUE, full_key, &key_size_bytes);
	if (res)
	{
		DMSG("TEE_GetObjectBufferAttribute failed %#" PRIx32, res);
		return res;
	}

	ed25519_signature signature;
	cardano_crypto_ed25519_sign(tx_id, TX_ID_SIZE, (unsigned char *)' ', 0, full_key, full_key + SECRET_KEY_SIZE + CHAIN_CODE_SIZE,
								signature);

	memcpy(params[1].memref.buffer, signature, SHA512_DIGEST_SIZE);
	IMSG("Transaction signed!");
	for (size_t i = 0; i < SHA512_DIGEST_SIZE; i++)
	{
		sprintf(sig_string + 2 * i, "%02x", signature[i]);
	}
	IMSG("Signature: %s", sig_string);

	return TEE_SUCCESS;
}

static TEE_Result generate_key(uint32_t param_types,
							   TEE_Param params[4])
{
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_OUTPUT,
											   TEE_PARAM_TYPE_NONE,
											   TEE_PARAM_TYPE_NONE,
											   TEE_PARAM_TYPE_NONE);
	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	// Generate a random wallet seed
	uint8_t seed[SEED_SIZE + 1];
	TEE_GenerateRandom(seed, SEED_SIZE);
	DMSG("Generated secure random seed");

	// Generate the wallet root private key from the seed
	size_t key_size_bytes = FULL_KEY_SIZE;
	uint8_t full_key[FULL_KEY_SIZE];
	// pw = "" (empty password, could use this to support "hidden" wallets)
	fastpbkdf2_hmac_sha512((const uint8_t *)"", 0, seed, SEED_SIZE, 4096, full_key, SECRET_KEY_SIZE + CHAIN_CODE_SIZE);
	DMSG("fastpbkdf2_hmac_sha512 generated root private key");

	// Tweak bits (Icarus):
	// * On the Ed25519 scalar leftmost 32 B:
	//   - Clear the lowest 3 bits
	//   - Clear the highest bit
	//   - Clear the 3rd highest bit
	//   - Set the 2nd highest bit
	full_key[0] &= 0b11111000;
	full_key[31] &= 0b00011111;
	full_key[31] |= 0b01000000;

	// Derive the wallet root public key from the private key
	cardano_crypto_ed25519_publickey(full_key, full_key + SECRET_KEY_SIZE + CHAIN_CODE_SIZE);
	DMSG("cardano_crypto_ed25519_publickey derived root public key");

	// Calculate the checksum of the seed
	uint8_t checksum[SHA256_DIGEST_SIZE];
	sha256(seed, SEED_SIZE, checksum);
	seed[SEED_SIZE] = checksum[0];

	// Compute the mnemonic indices from the seed by breaking it into 11-bit chunks
	const uint8_t WORD_SIZE_BITS = 11;
	const uint16_t WORD_INDEX_MASK = 0b0000011111111111;
	size_t ent_idx = 0;
	uint16_t carry_bits = 0, n_carry_bits = 0, n_bits = 0, ent_bits = 0;
	uint16_t *mnemonic_indices = (uint16_t *)malloc(MNEMONIC_SIZE * sizeof(uint16_t));

	for (size_t i = 0; i < MNEMONIC_SIZE; i++)
	{
		mnemonic_indices[i] = (uint16_t)(carry_bits << (WORD_SIZE_BITS - n_carry_bits));
		n_bits = n_carry_bits;

		while (n_bits < WORD_SIZE_BITS)
		{
			ent_bits = seed[ent_idx++];
			if (WORD_SIZE_BITS - n_bits >= 8)
				ent_bits <<= WORD_SIZE_BITS - n_bits - 8;
			else
				ent_bits >>= 8 - (WORD_SIZE_BITS - n_bits);
			mnemonic_indices[i] |= ent_bits;
			n_bits += 8;
		}

		n_carry_bits = n_bits - 11;
		carry_bits = seed[ent_idx - 1];
		carry_bits &= 255 >> (8 - n_carry_bits);
		mnemonic_indices[i] &= WORD_INDEX_MASK;
	}

	// Return the mnemonic indices to the caller
	memcpy(params[0].memref.buffer, mnemonic_indices, MNEMONIC_SIZE * 2);
	IMSG("Mnemonic successfully generated!");

	// Allocate an Ed25519 TEE key
	TEE_ObjectHandle transient_key = (TEE_ObjectHandle)NULL;
	TEE_ObjectHandle persistent_key = (TEE_ObjectHandle)NULL;
	TEE_Result res = TEE_ERROR_GENERIC;
	uint32_t flags = TEE_DATA_FLAG_ACCESS_READ | TEE_DATA_FLAG_ACCESS_WRITE | TEE_DATA_FLAG_ACCESS_WRITE_META | TEE_DATA_FLAG_SHARE_READ | TEE_DATA_FLAG_SHARE_WRITE;
	uint32_t objectID = 1;

	res = TEE_AllocateTransientObject(TEE_TYPE_HMAC_SHA256, (SECRET_KEY_SIZE + CHAIN_CODE_SIZE + PUBLIC_KEY_SIZE) * 8,
									  &transient_key);
	if (res)
	{
		DMSG("TEE_AllocateTransientObject failed %#" PRIx32, res);
		return res;
	}
	DMSG("TEE_AllocateTransientObject success");

	// Store the private and public keys in the transient object
	TEE_Attribute attrs[1] = {
		{
			.attributeID = TEE_ATTR_SECRET_VALUE,
			.content.ref.buffer = full_key,
			.content.ref.length = FULL_KEY_SIZE,
		},
	};
	res = TEE_PopulateTransientObject(transient_key, attrs, 1);
	DMSG("TEE_PopulateTransientObject success");

	// Print the full key
	char subkey_string[2 * key_size_bytes + 1];
	res = TEE_GetObjectBufferAttribute(transient_key, TEE_ATTR_SECRET_VALUE, full_key, &key_size_bytes);
	if (res)
	{
		DMSG("TEE_GetObjectBufferAttribute failed %#" PRIx32, res);
		return res;
	}

	// Print the private key
	for (size_t i = 0; i < SECRET_KEY_SIZE; i++)
	{
		sprintf(subkey_string + 2 * i, "%02x", full_key[i]);
	}
	subkey_string[2 * SECRET_KEY_SIZE] = '\0';
	IMSG("Private key: %s\n", subkey_string);

	// Print the chain code
	for (size_t i = 0; i < CHAIN_CODE_SIZE; i++)
	{
		sprintf(subkey_string + 2 * i, "%02x", full_key[SECRET_KEY_SIZE + i]);
	}
	subkey_string[2 * CHAIN_CODE_SIZE] = '\0';
	IMSG("Chain code: %s\n", subkey_string);

	// Print the public key
	for (size_t i = 0; i < PUBLIC_KEY_SIZE; i++)
	{
		sprintf(subkey_string + 2 * i, "%02x", full_key[SECRET_KEY_SIZE + CHAIN_CODE_SIZE + i]);
	}
	subkey_string[2 * PUBLIC_KEY_SIZE] = '\0';
	IMSG("Public key: %s\n", subkey_string);

	// Delete the persistent object if it already exists
	res = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE, &objectID, sizeof(objectID), flags, &persistent_key);
	if (res == TEE_SUCCESS)
	{
		IMSG("Persistent object already exists. Overwriting...");
		res = TEE_CloseAndDeletePersistentObject1(persistent_key);
		if (res != TEE_SUCCESS)
		{
			IMSG("Failed to close and delete the existing persistent key: 0x%x", res);
			return res;
		}
	}

	res = TEE_CreatePersistentObject(TEE_STORAGE_PRIVATE, &objectID, sizeof(objectID), flags, transient_key, NULL, 0, &persistent_key);
	if (res != TEE_SUCCESS)
	{
		IMSG("Failed to create persistent key: 0x%x", res);
		return res;
	}
	IMSG("Persistent object for Ed25519 key successfully created!\n");
	TEE_CloseObject(persistent_key);
	TEE_FreeTransientObject(transient_key);

	return TEE_SUCCESS;
}

/*
 * Called when a TA is invoked. sess_ctx hold that value that was
 * assigned by TA_OpenSessionEntryPoint(). The rest of the paramters
 * comes from normal world.
 */
TEE_Result TA_InvokeCommandEntryPoint(void __maybe_unused *sess_ctx,
									  uint32_t cmd_id,
									  uint32_t param_types, TEE_Param params[4])
{
	(void)&sess_ctx; /* Unused parameter */

	switch (cmd_id)
	{
	case TA_TRUSTED_SIGNER_CMD_SIGN_TX:
		return sign_tx(param_types, params);
	case TA_TRUSTED_SIGNER_CMD_GENERATE_KEY:
		return generate_key(param_types, params);
	default:
		return TEE_ERROR_BAD_PARAMETERS;
	}
}
