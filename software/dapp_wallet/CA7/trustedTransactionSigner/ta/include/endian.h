/*
 * endian.h
 *
 *  Created on: May 28, 2024
 *      Author: willie
 */

#ifndef TA_INCLUDE_ENDIAN_H_
#define TA_INCLUDE_ENDIAN_H_

#define LITTLE_ENDIAN 1234
#define __LITTLE_ENDIAN 1234
#define BYTE_ORDER LITTLE_ENDIAN
#define __BYTE_ORDER LITTLE_ENDIAN

#endif /* TA_INCLUDE_ENDIAN_H_ */
