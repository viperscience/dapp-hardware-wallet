# Skip build if libqcbor.a exists
if [ -f "QCBOR/build/libqcbor.a" ]; then
    echo "Skipping QCBOR build - libqcbor.a exists"
else
    cp -r ../QCBOR . && \
    cd QCBOR/build && \
    rm -rf * && \
    cmake -S .. -B . -DCMAKE_TOOLCHAIN_FILE=../QCBOR/toolchain-arm-none-eabi.cmake && \
    make
    cd ../..
fi

# Verify the output of the build

output=$(readelf -h QCBOR/build/libqcbor.a | grep 'Class\|File\|Machine' | awk '{print $2}' | sed -n '2,3p' | tr '\n' ' ' | sed 's/ *$//')
expected_output="ELF32 ARM"
if [ "$output" != "$expected_output" ]; then
    echo "libqcbor.a built for wrong target architecture: Expected '$expected_output', but got '$output'"
    exit 1
else
    echo "libqcbor.a built successfully."
    exit 0
fi
