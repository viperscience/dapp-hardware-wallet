#!/bin/bash

# Compiles U-BOOT, TF-A BL2, and OP-TEE for mp157f-dk2 with OP-TEE in SYSRAM and updates Starter Package
#   NB: This script expects that all sources are extracted with patches already applied and that the cross-compilation environment has been sourced

BASEDIR="${PWD}/stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi"
FIPDIR="${BASEDIR}/FIP_artifacts"
STARTER_PKG_DIR="${PWD}/../Starter-Package"

echo ""
echo "************************************************"
echo "**** STEP 1: CLEANING UP EXISTING ARTIFACTS ****"
echo "************************************************"
sleep 1
cd $FIPDIR
rm -rf arm-trusted-firmware/*
rm -rf fip/*
rm -rf optee/*
rm -rf u-boot/*
echo Done

echo ""
echo "******************************"
echo "**** STEP 2: BUILD U-BOOT ****"
echo "******************************"
sleep 1
cd "$BASEDIR/u-boot-stm32mp-v2022.10-stm32mp-r1-r0/u-boot-stm32mp-v2022.10-stm32mp-r1"
make stm32mp15_defconfig
make DEVICE_TREE=stm32mp157f-dk2 all
cp u-boot-nodtb.bin $FIPDIR/u-boot/u-boot-nodtb-stm32mp15.bin
cp u-boot.dtb $FIPDIR/u-boot/u-boot-stm32mp157f-dk2.dtb
echo Done


echo ""
echo "******************************"
echo "**** STEP 3: BUILD OP-TEE ****"
echo "******************************"
echo "NOTE: Ignore error at the end of build stage about missing TF-A files. These will be generated in the next step."
sleep 1
cd "$BASEDIR/optee-os-stm32mp-3.19.0-stm32mp-r1-r0/optee-os-stm32mp-3.19.0-stm32mp-r1"
make -f $PWD/../Makefile.sdk CFG_EMBED_DTB_SOURCE_FILE=stm32mp157f-dk2 CFG_STM32MP1_OPTEE_IN_SYSRAM=y CFG_STM32MP15_HUK=y CFG_STM32_HUK_FROM_DT=y DEPLOYDIR=$FIPDIR/optee FIP_DEPLOYDIR_ROOT=$FIPDIR all
echo Done


echo ""
echo "********************************"
echo "**** STEP 4: BUILD TF-A BL2 ****"
echo "********************************"
sleep 1
cd "$BASEDIR/tf-a-stm32mp-v2.8.6-stm32mp-r1-r0/tf-a-stm32mp-v2.8.6-stm32mp-r1"
make -f $PWD/../Makefile.sdk TF_A_DEVICETREE=stm32mp157f-dk2 STM32MP1_OPTEE_IN_SYSRAM=1 FIP_DEPLOYDIR_ROOT=$FIPDIR DEPLOYDIR=$FIPDIR/arm-trusted-firmware all
echo Done


echo ""
echo "****************************************"
echo "**** STEP 5: UPDATE STARTER PACKAGE ****"
echo "****************************************"
sleep 1
rm -rf $STARTER_PKG_DIR/stm32mp1/arm-trusted-firmware/*
rm -rf $STARTER_PKG_DIR/stm32mp1/fip/*
cd $FIPDIR
cp -rvf arm-trusted-firmware/* $STARTER_PKG_DIR/stm32mp1/arm-trusted-firmware/
cp -rvf fip/* $STARTER_PKG_DIR/stm32mp1/fip/
echo Done

