# dApp Hardware Wallet

A hardware wallet designed for live dApp services. 

*An early prototype device is shown below:*

![](docs/images/prototype.gif)

## Key Components

![](docs/images/key_components.jpg)

## Background

### What is a hardware wallet?

A hardware device that securely stores wallet private keys, and signs transactions without the need to reveal the private keys outside of the device.

![](docs/images/hw_wallet_diagram.jpg)

### How is the dApp Hardware Wallet different?

* Transactions are signed autonomously, allowing the device to be integrated with a live service
* Powerful Linux CPU, protected behind a restricted physical layer firewall, provides a drastic increase in onboard capabilities without compromising security
* Large touchscreen interface
    * Easier to input PIN
    * Dashboard displays status information
    * Configurable time window to cancel incoming transations

![](docs/images/dapp_hw_wallet_diagram.jpg)

### Why should I use this instead of a hot wallet running on my server?

Billions of dollars have been lost in hacks due to leaked wallet keys. Leaked keys are **permanently compromised**. Storing keys in a hardware device such that they can never leave the device prevents the possibility of keys being leaked in the event of an attack. Additionally, the customizable transaction guard that runs within the device can filter out abnormal transactions (e.g. wallet drain, high TX frequency). Altogether, these security measures greatly limit the scope of damage that an attack can incur.

## Transaction Signing Sequence

![](docs/images/sequence_diagram.jpg)