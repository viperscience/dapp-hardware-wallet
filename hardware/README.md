# Hardware

## Development Board

The first dApp wallet prototype utilizes the [STM32MP157F-DK2](https://www.st.com/en/evaluation-tools/stm32mp157f-dk2.html) discovery kit from ST Microelectronics.

![STM32MP157F-DK2](STM32MP157F-DK2.png)

The primary features of this board are:

* STM32MP157 dual-core ARM Cortex‑A7 CPU (800 MHz, 32 bits) + ARM Cortex‑M4 (32 bits) MPU
* 4-Gbit DDR3L, (533 MHz, 16 bits)
* ARM TrustZone secure execution context
* STM32 MPU OpenSTLinux Distribution
* 4" touchscreen display (480×800 pixels)

ARM TrustZone provides a trusted execution environment that isolates trusted applications from the rest of the device. The STM MPU includes firewall mechanisms to restrict read/write access of "secure world" peripherals from the "normal world".

![TrustZone Overview](Optee-components-diagram.png)